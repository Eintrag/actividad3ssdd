// -*- mode:c++ -*-

module drobots {

  interface Robot {
    bool cannon(int angle, int distance);
    int scan(int angle, int wide);
  };

  interface RobotController {
    void turn();
    void robotDestroyed();
    int evaluarPosicion();
    void avisarDisparoRealizado();
  };

};
